<?php

class Users extends Database
{
    function __construct($params)
    {
        parent::__construct();

        switch (count($params)) {
            case 5: //Employee commands
                switch ($params[2]) {
                    case 'login':
                        $this->login($params[4], true);
                        break;
                    case 'get':
                        $this->get($params[4], true);
                        break;
                    default:
                        $this->error(1002);
                }
                break;
            case 4: //Regular user commands
                switch ($params[2]) {
                    case 'add':
                        $this->add($params[3]);
                        break;
                    case 'addemp':
                        $this->addEmployee($params[3]);
                        break;
                    case 'del':
                        $this->delete($params[3]);
                        break;
                    case 'edit':
                        $this->edit($params[3]);
                        break;
                    case 'get':
                        $this->get($params[3]);
                        break;
                    case 'login':
                        $this->login($params[3]);
                        break;
                    case 'wachtwoordvergeten':
                        $this->wachtwoordVergeten($params[3]);
                        break;
                    default:
                        $this->error(1002);
                }
                break;
            case 3:
                switch ($params[2]) {
                    case 'get':
                        $this->get();
                        break;
                    default:
                        $this->error(1002);
                }
                break;
            default:
                $this->error(1002);
        }
    }

    function add($params)
    {
        $params = explode(',', $params);
        if (count($params) == 12) {

            //Get all values from parameter string
            $voornaam = ucwords(strtolower($params[0]));
            $tussenvoegsel = strtolower($params[1]);
            $achternaam = ucwords(strtolower($params[2]));
            $geslacht = strtolower($params[3]) == 'm' ? 'm' : 'v';
            $postcode = strtoupper(str_replace(' ','',$params[4]));
            $straat = ucwords(strtolower($params[5]));
            $huisnummer = $params[6];
            $plaats = ucwords(strtolower($params[7]));
            $email = $params[8];
            $telefoon = $params[9];
            $geboortedatum = date("Y-m-d", strtotime($params[10]));
            $wachtwoord = md5($params[11]);

            //Check if email is already registered
            $check = $this->execute("SELECT email FROM " . GEBRUIKERS . " WHERE email = '" . $email . "'");
            if (!empty($check)) {
                $this->error(2001);
            }

            //Add user to the database
            $query = "INSERT INTO " . GEBRUIKERS . " (voornaam, tussenvoegsel, achternaam, geslacht, postcode, straat, huisnummer, plaats, email, telefoon, geboortedatum, wachtwoord) ";
            $query .= "VALUES ('" . $voornaam . "','" . $tussenvoegsel . "','" . $achternaam . "','" . $geslacht . "','" . $postcode . "','" . $straat . "','" . $huisnummer . "','" . $plaats . "','" . $email . "','" . $telefoon . "','" . $geboortedatum . "','" . $wachtwoord . "')";
            $this->execute($query);

            //Retrieve added user information to check if successfully added and for login purposes
            $result = $this->execute("SELECT * FROM " . GEBRUIKERS . " WHERE email = '" . $email . "'");
            if (empty($result)) {
                $this->error(1006);
            }
            print_r(json_encode($result));
            exit();
        } else {
            $this->error(1004);
        }
    }

    function addEmployee($params) {
        $params = explode(',', $params);
        if (count($params) == 13) {

            //Get all values from parameter string
            $voornaam = ucwords(strtolower($params[0]));
            $tussenvoegsel = strtolower($params[1]);
            $achternaam = ucwords(strtolower($params[2]));
            $geslacht = strtolower($params[3]) == 'm' ? 'm' : 'v';
            $postcode = strtoupper(str_replace(' ','',$params[4]));
            $straat = ucwords(strtolower($params[5]));
            $huisnummer = $params[6];
            $plaats = ucwords(strtolower($params[7]));
            $email = $params[8];
            $telefoon = $params[9];
            $geboortedatum = date("Y-m-d", strtotime($params[10]));
            $wachtwoord = md5($params[11]);
            $function_id = $params[12];

            //Check if email is already registered
            $check = $this->execute("SELECT email FROM " . GEBRUIKERS . " WHERE email = '" . $email . "'");
            if (!empty($check)) {
                $this->error(2001);
            }

            //Add user to the database
            $query = "INSERT INTO " . GEBRUIKERS . " (voornaam, tussenvoegsel, achternaam, geslacht, postcode, straat, huisnummer, plaats, email, telefoon, geboortedatum, wachtwoord, medewerker) ";
            $query .= "VALUES ('" . $voornaam . "','" . $tussenvoegsel . "','" . $achternaam . "','" . $geslacht . "','" . $postcode . "','" . $straat . "','" . $huisnummer . "','" . $plaats . "','" . $email . "','" . $telefoon . "','" . $geboortedatum . "','" . $wachtwoord . "', '1')";
            $this->execute($query);

            //Retrieve added user information to check if successfully added and for login purposes
            $result = $this->execute("SELECT id FROM " . GEBRUIKERS . " WHERE email = '" . $email . "'");
            if (empty($result)) {
                $this->error(1006);
            }
            print_r(json_encode($result));

            //Add employees function to the employees table
            $query = "INSERT INTO ".MEDEWERKERS." (gebruikers_id, functie_id) VALUES ('".$result[0]['id']."','".$function_id."')";
            $this->execute($query);

            exit();
        } else {
            $this->error(1004);
        }
    }

    function edit($params)
    {
        $params = explode(',', $params);
        if (count($params) == 14) {

            //Get all values from parameter string
            $id = $params[0];
            $voornaam = ucwords(strtolower($params[1]));
            $tussenvoegsel = strtolower($params[2]);
            $achternaam = ucwords(strtolower($params[3]));
            $geslacht = strtolower($params[4]) == 'm' ? 'm' : 'v';
            $postcode = strtoupper(str_replace(' ', '', $params[5]));
            $straat = ucwords(strtolower($params[6]));
            $huisnummer = $params[7];
            $plaats = ucwords(strtolower($params[8]));
            $email = $params[9];
            $telefoon = $params[10];
            $geboortedatum = date("Y-m-d", strtotime($params[11]));
            $wachtwoord = md5($params[12]);
            $functieid = $params[13];

            //Build the query
            $query = "UPDATE " . GEBRUIKERS . " SET voornaam = '" . $voornaam . "',
            tussenvoegsel = '" . $tussenvoegsel . "',
            achternaam = '" . $achternaam . "',
            geslacht = '" . $geslacht . "',
            postcode = '" . $postcode . "',
            straat = '" . $straat . "',
            huisnummer = '" . $huisnummer . "',
            plaats = '" . $plaats . "',
            email = '" . $email . "',
            telefoon = '" . $telefoon . "',
            geboortedatum = '" . $geboortedatum . "'";

            if($wachtwoord != md5('')) {
                $query .= ", wachtwoord = '" . $wachtwoord . "'";
            }

            //Check if user was made an employee
            if ($functieid != 0) {
                $query .= ", medewerker = 1";
            }
            $query .= " WHERE id = $id";
            $this->execute($query);

            //Update an employee's function
            if ($functieid != 0) {
                $check = $this->execute("SELECT * FROM " . MEDEWERKERS . " WHERE gebruikers_id = '" . $id . "'");
                if (empty($check)) {
                    $this->execute("INSERT INTO " . MEDEWERKERS . " (gebruikers_id, functie_id) VALUES ('" . $id . "','" . $functieid . "')");
                } else {
                    $this->execute("UPDATE " . MEDEWERKERS . " SET functie_id = '" . $functieid . "' WHERE gebruikers_id = '" . $id . "'");
                }
            }

            //Retrieve the updated user
            $query = "SELECT * FROM ".GEBRUIKERS." WHERE id = '".$id."'";
            $result = $this->execute($query);
            if(empty($result)){
                $this->error(1000);
            }
            print_r(json_encode($result));
            exit();
        } else {
            $this->error(1004);
        }

    }

    function get($ids = null, $admin = false)
    {
        //Check if an employee is being retrieved
        if ($ids == 'admin') {
            $ids = null;
            $admin = true;
        }
        //Build the query
        $query = "SELECT * FROM " . GEBRUIKERS . " a ";
        if ($admin) {
            //Retrieve the employee's function
            $query .= "INNER JOIN medewerkers b ON a.id = b.gebruikers_id INNER JOIN functies c ON b.functie_id = c.id ";
        }
        //Expand the query further
        if ($admin && $ids) {
            $query .= "WHERE a.medewerker = 1 AND a.id IN ($ids)";
        } else {
            if ($admin) {
                $query .= "WHERE a.medewerker = 1";
            }
            if ($ids) {
                $query .= "WHERE a.id IN ($ids)";
            }
        }

        $result = $this->execute($query);
        if (empty($result)) {
            $this->error(1005);
        }
        print_r(json_encode($result));
        exit();
    }

    function delete($ids)
    {
        //Queries to delete from tables by id's
        $query = "DELETE FROM " . GEBRUIKERS . " WHERE id IN ($ids);";
        $query .= "DELETE FROM " . MEDEWERKERS . " WHERE id IN ($ids)";
        $this->execute($query);

        //Try to retreive the deleted id's incase they were not deleted properly
        $query = "SELECT id FROM gebruikers WHERE id IN ($ids)";
        $result = $this->execute($query);
        if ($result) {
            $this->error(2002);
        }
        //If nothing is retreived the users were deleted successfully
        $this->success(401);
    }

    function login($params, $admin = false)
    {
        $params = explode(',', $params);
        if (count($params) == 2) {
            //Get all values from parameter string
            $email = $params[0];
            $password = md5($params[1]);

            //Build the query and add to it if we're dealing with an admin login
            $query = "SELECT * FROM " . GEBRUIKERS . " WHERE email = '" . $email . "' AND wachtwoord = '" . $password . "'";
            if ($admin) {
                $query .= ' AND medewerker = 1';
            }

            $result = $this->execute($query);
            //Check if executed query returned values
            if (!empty($result)) {
                //Display the result as JSON string for XML requests
                print_r(json_encode($result));
                exit();
            } else {
                $this->error(1005);
            }
        } else {
            $this->error(1004);
        }
    }

    function wachtwoordVergeten($email) {
        $query = "SELECT id FROM ".GEBRUIKERS." WHERE email = '".$email."'";
        $result = $this->execute($query);
        if($result){
            $wachtwoord = rand(10000, 99999);
            $query = "UPDATE ".GEBRUIKERS." SET wachtwoord = '".md5($wachtwoord)."' WHERE email = '".$email."'";
            $this->execute($query);
            $this->success(638,$wachtwoord);
        } else {
            $this->error(5638,"Emailadres komt niet voor in onze database");
        }
    }

}
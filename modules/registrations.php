<?php

class Registrations extends Database
{
    function __construct($params)
    {
        parent::__construct();

        switch (count($params)) {
            case 4:
                switch ($params[2]) {
                    case 'add':
                        $this->add($params[3]);
                        break;
                    case 'edit':
                        $this->edit($params[3]);
                        break;
                    case 'del':
                        $this->del($params[3]);
                        break;
                    case 'get':
                        $this->get($params[3]);
                        break;
                    default:
                        $this->error(1002);
                }
                break;
            case 3:
                switch ($params[2]) {
                    case 'get':
                        $this->get();
                        break;
                    default:
                        $this->error(1002);
                }
                break;
            default:
                $this->error(1002);
        }
    }

    private function add($params)
    {
        $params = explode(',', $params);
        if (count($params) == 2) {
            $gebruikers_id = $params[0];
            $cursus_id = $params[1];

            $query = "INSERT INTO " . INSCHRIJVINGEN . " (gebruikers_id, cursus_id) VALUES ('" . $gebruikers_id . "','" . $cursus_id . "')";
            $this->execute($query);
            $this->success(402);
        } else {
            $this->error(1004);
        }
    }

    private function edit($params)
    {
        $params = explode(',', $params);
        if (count($params) == 3) {
            $id = $params[0];
            $gebruikers_id = $params[1];
            $cursus_id = $params[2];

            $query = "UPDATE " . INSCHRIJVINGEN . " SET gebruikers_id = '" . $gebruikers_id . "', cursus_id = '" . $cursus_id . "' WHERE id = $id";
            $this->execute($query);
            $this->success(403);
        } else {
            $this->error(1004);
        }
    }

    private function del($ids)
    {
        $query = "DELETE FROM " . INSCHRIJVINGEN . " WHERE id IN ($ids)";
        $this->execute($query);

        //Try to retrieve the deleted id's incase they were not deleted properly
        $check = $this->execute("SELECT * FROM " . INSCHRIJVINGEN . " WHERE id IN ($ids)");
        if ($check) {
            $this->error(1000);
        }
        //If nothing is retrieved the registrations were deleted successfully
        $this->success(401);
    }

    private function get($filters = null)
    {
        $query = "SELECT * FROM " . INSCHRIJVINGEN . " a INNER JOIN " . CURSUS_TYPES . " b ON a.cursus_id = b.id INNER JOIN " . GEBRUIKERS . " c ON a.gebruikers_id = c.id";

        if ($filters) {
            $query .= " WHERE ";
            $filters = json_decode($filters,true);
            print_r($filters);
        }

        echo $query . '</br>';
    }
}
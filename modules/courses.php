<?php

class Courses extends Database
{

    function __construct($params)
    {
        parent::__construct();

        switch (count($params)) {
            case 4:
                switch ($params[2]) {
                    case 'add':
                        $this->add($params[3]);
                        break;
                    case 'del':
                        $this->del($params[3]);
                        break;
                    case 'edit':
                        $this->edit($params[3]);
                        break;
                    case 'get':
                        $this->get($params[3]);
                        break;
                    default:
                        $this->error(1002);
                }
                break;
            case 3:
                switch ($params[2]) {
                    case 'get':
                        $this->get();
                        break;
                    default:
                        $this->error(1002);
                }
                break;
            default:
                $this->error(1002);
        }

    }

    private function add($params)
    {
        $params = explode(',', $params);
        if (count($params) == 2) {
            //Get all values from parameter string
            $type = $params[0];
            $weeknummer = $params[1];

            $query = "INSERT INTO " . CURSUS_INGEPLAND . " (type_id, weeknummer) VALUES ('" . $type . "','" . $weeknummer . "')";
            $this->execute($query);
            $this->success(402);
        } else {
            $this->error(1004);
        }
    }

    private function edit($params)
    {
        $params = explode(',', $params);
        if (count($params) == 3) {
            //Get all values from parameter string
            $id = $params[0];
            $type = $params[1];
            $weeknummer = $params[2];

            $query = "UPDATE " . CURSUS_INGEPLAND . " SET type_id = '" . $type . "', weeknummer = '" . $weeknummer . "' WHERE id = '" . $id . "'";
            $this->execute($query);
            $this->success(403);
        } else {
            $this->error(1004);
        }
    }

    private function del($ids)
    {
        $query = "DELETE FROM " . CURSUS_INGEPLAND . " WHERE id IN ($ids)";
        $this->execute($query);

        //Try to retreive the deleted id's incase they were not deleted properly
        $check = $this->execute("SELECT id FROM " . CURSUS_INGEPLAND . " WHERE id IN ($ids)");
        if ($check) {
            $this->error(1000);
        }
        //If nothing is retreived the courses were deleted successfully
        $this->success(401);
    }

    private function get($filters = null)
    {
        //Build the query
        $query = "SELECT a.id AS 'cursus_id', a.type_id AS 'cursus_type_id', a.weeknummer, b.naam, b.prijs FROM " . CURSUS_INGEPLAND . " a INNER JOIN " . CURSUS_TYPES . " b ON a.type_id = b.id";
        if ($filters) {
            $filters = json_decode($filters, true);
            if(is_array($filters)) {
                $query .= " WHERE";
                foreach ($filters as $key => $value) {
                    $query .= " `$key` = '" . $value . "' AND";
                }
                $query = substr($query, 0, -3);
            } else {
                $this->error(1004);
            }
        }
        echo $query . '<br/>';
        $result = $this->execute($query);
        if(!$result){
            $this->error(1005);
        }
        print_r(json_encode($result));
        exit();
    }

}
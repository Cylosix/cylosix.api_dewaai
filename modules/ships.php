<?php

class Ships extends Database
{
    function __construct($params)
    {
        parent::__construct();

        switch (count($params)) {
            case 4:
                switch ($params[2]) {
                    case 'add':
                        $this->add($params[3]);
                        break;
                    case 'del':
                        $this->del($params[3]);
                        break;
                    case 'edit':
                        $this->edit($params[3]);
                        break;
                    case 'get':
                        $this->get($params[3]);
                        break;
                    default:
                        $this->error(1002);
                }
                break;
            case 3:
                switch ($params[2]) {
                    case 'get':
                        $this->get();
                        break;
                    default:
                        $this->error(1002);
                }
                break;
            default:
                $this->error(1002);
        }
    }

    function add($params)
    {
        $params = explode(',', $params);
        if (count($params) == 2) {
            //Get all values from parameter string
            $type = $params[0];
            $naam = $params[1];

            $query = "INSERT INTO " . SCHEPEN . " (type_id, naam) VALUES ('" . $type . "','" . $naam . "')";
            $this->execute($query);
            $this->success(302);
        } else {
            $this->error(1004);
        }
    }

    function edit($params)
    {
        $params = explode(',', $params);
        if (count($params) == 4) {
            //Get all values from parameter string
            $id = $params[0];
            $type = $params[1];
            $naam = $params[2];
            $invaart = $params[3];

            $query = "UPDATE " . SCHEPEN . " SET type_id = '" . $type . "', naam = '" . $naam . "', in_vaart = '" . $invaart . "' WHERE id = '" . $id . "'";
            $this->execute($query);
            $this->success(303);
        } else {
            $this->error(1004);
        }
    }

    function del($ids)
    {
        $query = "DELETE FROM " . SCHEPEN . " WHERE id IN ($ids)";
        $this->execute($query);

        //Try to retreive the deleted id's incase they were not deleted properly
        $check = $this->execute("SELECT id FROM " . SCHEPEN . " WHERE id IN ($ids)");
        if ($check) {
            $this->error(4002);
        }
        //If nothing is retreived the ships were deleted successfully
        $this->success(301);
    }

    function get($ids = null)
    {
        //Build the query
        $query = "SELECT a.id, type_id, b.type, naam, capaciteit, in_vaart FROM " . SCHEPEN . " a INNER JOIN " . SCHIP_TYPES . " b ON a.type_id = b.id";
        if ($ids) {
            $query .= " WHERE a.id IN ($ids)";
        }

        $result = $this->execute($query);
        if (empty($result)) {
            $this->error(1005);
        }
        print_r(json_encode($result));
        exit();
    }
}
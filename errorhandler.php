<?php

class ErrorHandler
{
    //Array to store error codes and messages
    private $messages = array(
        100 => 'Unspecified success',

        401 => 'Successfully deleted',
        402 => 'Successfully added',
        403 => 'Successfully updated',

        999 => 'Database connection error',

        1000 => 'Unspecified error',
        1001 => 'Incorrect API-key',
        1002 => 'Incorrect request',
        1003 => 'Incorrect module',
        1004 => 'Incorrect parameters',
        1005 => 'Query yielded no results',
        1006 => 'Query contains incorrect information',

        2001 => 'This email is already registered',
        2002 => 'User(s) not deleted correctly',


        4002 => 'Ship(s) not deleted correctly',
        4003 => 'Ship not was not added to the database',
        4052 => 'Ship type(s) not deleted correctly',
        4053 => 'Ship type was not added to the database'
    );

    function error($code, $msg = '')
    {
        if (!array_key_exists($code, $this->messages)) {
            $code = 1000;
        }
        if ($msg == '') {
            $msg = $this->messages[$code];
        }

        //Stop execution on error and display an error as JSON
        die('{"error":{"code":"' . $code . '","message":"' . $msg . '"}}');
    }

    function success($code, $msg = '')
    {
        if(!array_key_exists($code, $this->messages)){
            $code = 100;
        }
        if($msg == '') {
            $msg = $this->messages[$code];
        }

        //Stop execution on error and display a success message as JSON
        die('{"success":{"code":"'.$code.'","message":"'.$msg.'"}}');
    }
}

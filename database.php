<?php

class Database extends ErrorHandler
{
    private $conn;

    public function __construct(){
        try{
            $this->conn = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS);
        } catch (PDOException $e){
            $this->error(999, $e->getMessage());
        }
    }

    public function execute($query) {
        try {
            //Prepare the query to prevent SQL-injection
            $result = $this->conn->prepare($query);
            $result->execute();
            return $result->fetchAll(PDO::FETCH_ASSOC);
        }catch(Exception $e){
            $this->error(999, $e->getMessage());
        }
        return null;
    }

}



$ships = file_get_contents(API_URL.'/ships/get');
$ships = json_decode($ships,true);
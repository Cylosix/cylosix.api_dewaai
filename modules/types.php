<?php

class Types extends Database
{
    private $tables = array(
        'ship_types' => SCHIP_TYPES,
        'functies' => FUNCTIES,
        'course_types' => CURSUS_TYPES
    );

    private $table = null;
    private $cols = array();

    function __construct($params, $table)
    {
        parent::__construct();
        //Set the table to be used depending on url module
        $this->table = $this->tables[$table];
        //Get the columns from the database for the corresponding table
        $cols = $this->execute("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" . $this->table . "'");
        foreach ($cols as $row) {
            array_push($this->cols, $row['COLUMN_NAME']);
        }

        switch (count($params)) {
            case 4:
                switch ($params[2]) {
                    case 'add':
                        $this->add($params[3]);
                        break;
                    case 'edit':
                        $this->edit($params[3]);
                        break;
                    case 'del':
                        $this->del($params[3]);
                        break;
                    case 'get':
                        $this->get($params[3]);
                        break;
                    default:
                        $this->error(1002);
                }
                break;
            case 3:
                switch ($params[2]) {
                    case 'get':
                        $this->get();
                        break;
                    default:
                        $this->error(1002);
                }
                break;
            default:
                $this->error(1002);
        }
    }

    private function add($params)
    {
        $params = explode(',', $params);
        if (count($params) == count($this->cols) -1) {
            //Build the query
            $query = "INSERT INTO " . $this->table . " (";
            //Loop for adding columns to the query
            for($i = 1; $i < count($this->cols); $i++) {
                $query .= $this->cols[$i];
                if($i < count($this->cols) -1) { $query .= ','; }
            }
            $query .= ") VALUES (";
            //Loop for adding params to the query
            for($i = 0; $i < count($params); $i++) {
                if($params[$i] == '') { $this->error(1004); }
                $query .= "'".$params[$i]."'";
                if($i < count($params) -1) { $query .= ','; }
            }
            $query .= ")";

            $this->execute($query);
            $this->success(352);
        } else {
            $this->error(1004);
        }
    }

    private function edit($params)
    {
        $params = explode(',', $params);
        if (count($params) == count($this->cols)) {
            //Build the query
            $query = "UPDATE " . $this->table . " SET ";
            //Loop for adding columns and params to the query
            for($i = 1; $i < count($params); $i++) {
                $query .= $this->cols[$i]." = '".$params[$i]."'";
                if($i < count($params) -1) { $query .= ', '; }
            }
            $query .= " WHERE ".$this->cols[0]." = " . $params[0];

            $this->execute($query);
            $this->success(353);
        } else {
            $this->error(1004);
        }
    }

    private function del($ids)
    {
        $query = "DELETE FROM " . $this->table . " WHERE id IN ($ids)";
        $this->execute($query);

        //Try to retreive the deleted id's incase they were not deleted properly
        $check = $this->execute("SELECT id FROM " . $this->table . " WHERE id IN ($ids)");
        if ($check) {
            $this->error(4052);
        }
        //If nothing is retreived the ships were deleted successfully
        $this->success(351);
    }

    private function get($ids = null)
    {
        //Build the query
        $query = "SELECT * FROM " . $this->table;
        if ($ids) {
            $query .= " WHERE id IN ($ids)";
        }

        $result = $this->execute($query);
        if (empty($result)) {
            $this->error(1005);
        }
        print_r(json_encode($result));
        exit();
    }
}
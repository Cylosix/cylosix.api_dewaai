<?php
//Include other required files
require 'defines.php';
require 'errorhandler.php';
require 'database.php';

//Enable useful error messages
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);

//Instantiate classes
$err = new ErrorHandler();

date_default_timezone_set('Europe/Amsterdam');

if (isset($_GET['params'])) {
    //Split given url parameter into array
    $params = explode('/', $_GET['params']);
    $module = null;

    //Restrict access without correct api key
    if ($params[0] == API_KEY) {   //Check for correct minimum number of parameter
        if (count($params) > 2) {   //Check which model should be instantiated
            switch ($params[1]) {
                case 'ship':
                    require 'modules/ships.php';
                    $module = new Ships($params);
                    break;
                case 'ship_types':
                case 'functies':
                case 'course_types':
                    require 'modules/types.php';
                    $module = new Types($params, $params[1]);
                    break;
                case 'course':
                    require 'modules/courses.php';
                    $module = new Courses($params);
                    break;
                case 'user':
                    require 'modules/users.php';
                    $module = new Users($params);
                    break;
                case 'reg':
                    require 'modules/registrations.php';
                    $module = new Registrations($params);
                    break;
                case 'invoice':
                    require 'modules/invoices.php';
                    $module = new Invoices($params);
                    break;
                default:
                    $err->error(1003);
            }
        } else {
            $err->error(1002);
        }
    } else {
        $err->error(1001);
    }
} else {
    $err->error(1001);
}